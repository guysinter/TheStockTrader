
const state = {
    currentFunds : 1000,
    stocks: []
};

const getters = {
    funds: state => {
        return state.currentFunds;
    },
    stockPortfolio(state, getters){
        return state.stocks.map(stock=> {
            //taking the stock srom the stocks midulke
            const record =getters.stocks.find(element => element.id == stock.id);
            return{
                id: stock.id,
                quantity: stock.quantity,
                name: record.name,
                price: record.price
            };
        });
    }
};

const mutations = {
    //extract from order object
    'BUY_STOCK'(state, { stockId, quantity, stockPrice}){
        //search if we already have this stock
        const record = state.stocks.find(element =>
            element.id == stockId);
        if(record){
            record.quantity +=quantity;
        }
        else{
            state.stocks.push({id: stockId,
                quantity: quantity, });
        }
        state.currentFunds -= quantity*stockPrice;
    },
    'SELL_STOCK'(state, { stockId, quantity, stockPrice}){
        const record = state.stocks.find(element =>
            element.id == stockId);
        if(record.quantity > quantity){
            record.quantity -=quantity;
        }
        else {
            state.stocks.splice(state.stocks.indexOf(record),1)
        }
        state.currentFunds += quantity*stockPrice;
    },
    'SET_PORTFOlIO'(state, portfolio) {
        state.currentFunds =  portfolio.funds;
        state.stocks = portfolio.stockPortfolio ? portfolio.stockPortfolio : []
    }
};

const  actions = {
    sellStock({commit}, order){
        commit('SELL_STOCK', order);
    }
};

export default {
    state,
    mutations,
    getters,
    actions
}
